/* eslint-disable no-unused-vars */
import dotenv from 'dotenv'
import { apm } from 'av-core-express/lib/handlers'
import http from 'http'
import app from '@root/app'

const env = process.env.NODE_ENV || 'local'
const PORT = process.env.PORT || 8080
// eslint-disable-next-line no-unused-expressions
const envConfig = dotenv.config({ path: `./env/${env}` }).parsed

export const server = http.createServer(app)
server.listen(PORT, () => {
  if (process.env.NODE_ENV === 'production') {
    console.log('APM active status:', apm.getInstance().elasticApm.isStarted())
  }
  console.log(`Server running at port ${PORT} in ${env}`)
})
