/* eslint-disable promise/param-names */
/* eslint-disable no-unused-vars */
import { RedisClientInstance, CacheRedis, IConfigRedisClient, IClusterRedis } from 'av-core-infrastructure'
import dotenv from 'dotenv'

const env = process.env.NODE_ENV || 'local'
const envConfig = dotenv.config({ path: `./env/${env}` }).parsed

const redisInstance = (): RedisClientInstance | undefined => {
  if (process.env.REDIS_PORT && (process.env.REDIS_SERVER || process.env.REDIS_CLUSTER)) {
    const port: number = parseInt(process.env.REDIS_PORT)
    const pass: string = process.env.REDIS_PASS || ''
    const server: string = process.env.REDIS_SERVER || ''
    const cluster: IClusterRedis[] | [] = process.env.REDIS_CLUSTER ? process.env.REDIS_CLUSTER.split(',').map((host: string) => { return { host, port } }) : []

    const configRedis: IConfigRedisClient = {
      cluster,
      pass,
      port,
      server
    }
    const client = new RedisClientInstance(configRedis).connect()
    client.on('ready', () => console.log('Redis connected'))
    client.on('error', function () {
      throw Error('Redis not connected')
    })
    return client
  }
  throw Error('REDIS INVALID PARAMETERS')
}

const clientRedis = redisInstance()

const initializeCache = (): CacheRedis => {
  const expired: number = parseInt(process.env.REDIS_EXPIRED || '3600')
  const db: string = process.env.REDIS_DB || 'CONWAT:'

  const cache = new CacheRedis(clientRedis, expired, db)

  return cache
}
const cache = initializeCache()

export default cache
