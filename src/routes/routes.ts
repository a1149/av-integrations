import Router from 'av-core-express/lib/router'
import { statusControler, integrationsController } from '@controllers'
import busApiController from '@root/controllers/busApiController'

Router.route('/live').get(statusControler)

Router.route('/integrations').get(integrationsController)

Router.route('/bus-api').get(busApiController)

export default Router
