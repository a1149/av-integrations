import { CHANNEL_APPM } from '@root/app/constant'
import { IStep } from '@root/app/interfaces'

export default (step: IStep, channel: string, objReplace: any): string => {
  let text = channel === CHANNEL_APPM && step.textApp ? step.textApp : step.text
  for (const key in objReplace) {
    text = text.split(key).join(objReplace[key])
  }
  return text
}
