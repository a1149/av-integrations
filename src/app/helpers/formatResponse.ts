import { IIntegrationResponse } from '@root/app/interfaces'
import { IOptions } from '@root/app/interfaces/IStep'

export default async (text: string, derivate = false, options: IOptions[] = [], activeSecuentialDialogue = false): Promise<IIntegrationResponse> => {
  const response: IIntegrationResponse = {
    text,
    options: options.length ? options : null,
    derivate,
    activeSecuentialDialogue
  }
  return response
}
