import Joi from 'joi'

export default async (data: any): Promise<any> => {
  const schema = Joi.object().keys({
    sessionId: Joi.string().required(),
    integration: Joi.string().required(),
    nup: Joi.number().required(),
    channel: Joi.string().required()
  })

  return Joi.assert(data, schema)
}
