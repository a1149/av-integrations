import Joi from 'joi'

export default async (data: any): Promise<any> => {
  const schema = Joi.object().keys({
    integration: Joi.string().required(),
    channel: Joi.any().optional(),
    branchId: Joi.string().required(),
    origin: Joi.string().required()
  })

  return Joi.assert(data, schema)
}
