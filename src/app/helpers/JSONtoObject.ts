export default (json: any): any => {
  return JSON.parse(JSON.stringify(json))
}
