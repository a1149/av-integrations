import { ICard } from '@root/app/interfaces'

export default async (context: any): Promise<ICard[]> => {
  const {
    marcTarjV: hasVisa,
    marcTarjA: hasAmex,
    marcTarjM: hasMaster
  } = context

  const listCards: ICard[] = [
    {
      active: hasVisa,
      title: 'Visa',
      name: 'VISA',
      numberKey: 'tarjV',
      accountKey: 'ctaTV',
      number: 0,
      account: 0,
      lastNumbers: ''
    },
    {
      active: hasAmex,
      title: 'American Express',
      name: 'AMEX',
      numberKey: 'tarjA',
      accountKey: 'ctaTA',
      number: 0,
      account: 0,
      lastNumbers: ''
    },
    {
      active: hasMaster,
      title: 'MasterCard',
      name: 'MASTER',
      numberKey: 'tarjM',
      accountKey: 'ctaTM',
      number: 0,
      account: 0,
      lastNumbers: ''
    }
  ]
  const cards = []
  for (const i in listCards) {
    const { active, numberKey, accountKey, name } = listCards[i]

    if (active) {
      const number = context[numberKey]
      listCards[i].number = number
      listCards[i].account = context[accountKey]
      listCards[i].lastNumbers = name === 'AMEX' ? 'XXXX-' + number.substr(10, 5) : 'XXXX-' + number.substr(12, 4)
      cards.push(listCards[i])
    }
  }
  return cards
}
