/* eslint-disable camelcase */
export interface IGetBusApiReq {
  branchId: string,
  channel: string,
  origin: string
}
