import { formatResponse, JSONtoObject, textReplace } from '@root/app/helpers'
import { IIntegrationResponse, IStep, ITelResponse } from '@root/app/interfaces'
import formatBusApiResponse from '@root/app/modules/busApi/formatBusApiResponse'
import { getAddressInfo, getBranchInfo, getTelInfo } from '@root/app/services/busApiService'
import JSONSteps from './steps'
import { IGetBusApiReq } from './IGetBusApiReq'

export default async (req: IGetBusApiReq): Promise<IIntegrationResponse> => {
  const { branchId, channel, origin } = req

  const busInformation = await getBranchInfo(branchId)
  if (busInformation.addressId) {
    busInformation.address = await getAddressInfo(busInformation.addressId)
  }
  if (busInformation.address) {
    const telList = await getTelInfo(busInformation.telephoneNumberIds)
    const objReplace = await formatBusApiResponse(busInformation, telList)
    const step = getStep(telList, origin)
    const formattedString = textReplace(step, channel, objReplace)
    return formatResponse(formattedString)
  } else throw new Error('no hay address en busInformation')
}

function getStep(telList: ITelResponse[] | undefined, origin: string): IStep {
  const step = JSONtoObject(JSONSteps)[origin]
  if (step) {
    return step
  } else {
    return telList?.length ? JSONtoObject(JSONSteps)[origin + 'Ok'] : JSONtoObject(JSONSteps)[origin + 'NoTel']
  }
}
