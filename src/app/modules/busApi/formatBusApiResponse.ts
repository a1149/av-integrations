import { IBusApiResponse, ITelResponse } from '@root/app/interfaces'
import { OpeningHour } from '@root/app/interfaces/IBusApi'

export default async (req: IBusApiResponse, telList: ITelResponse[] = []): Promise<any> => {
  const [propName, telephones] = telForRequest(telList)
  const objReplace = {
    '{$number}': req.legacyReference,
    '{$name}': req.name,
    '{$street}': req.address.streetName,
    '{$buildingNumber}': req.address.buildingNumber,
    '{$state}': req.address.state.code,
    '{$zipCode}': req.address.zipCode,
    '{$propName}': propName,
    '{$telephones}': telephones,
    '{$hours}': hoursForRequest(req.openingHours),
    '{$latitude}': req.address.coordinates.latitude,
    '{$longitude}': req.address.coordinates.longitude
  }
  return objReplace
}

function telForRequest(list: ITelResponse[]): string[] {
  const formattedTelList: string[] = []
  list.forEach(e => {
    const tel = ' (' + e.areaCode + ') ' + e.number
    formattedTelList.push(tel)
  })
  const telephones = formattedTelList.join(' /')
  const propName = (formattedTelList.length > 1 ? 'Teléfonos:' : 'Teléfono:')
  return [propName, telephones]
}

function hoursForRequest(hours: OpeningHour[]): string {
  const sorter: any = {
    monday: 1,
    tuesday: 2,
    wednesday: 3,
    thursday: 4,
    friday: 5,
    saturday: 6,
    sunday: 7
  }
  const translation: any = {
    monday: 'Lunes',
    tuesday: 'Martes',
    wednesday: 'Miércoles',
    thursday: 'Jueves',
    friday: 'Viernes',
    saturday: 'Sábado',
    sunday: 'Domingo'
  }

  hours.sort(function sortByDay(a, b) {
    const day1 = a.day.toLowerCase()
    const day2 = b.day.toLowerCase()
    return sorter[day1] - sorter[day2]
  })

  const openingDay = translation[hours[0].day.toLowerCase()]
  const closingDay = translation[hours[hours.length - 1].day.toLowerCase()]
  const openingHour = hours[0].openingHour
  const closingHour = hours[0].closingHour

  const response = openingDay + ' a ' + closingDay + ' de ' + openingHour + ' a ' + closingHour
  return response
}
