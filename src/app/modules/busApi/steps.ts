export default {
  ejecutivoOk: {
    name: 'ejecutivoOk',
    text: '<b>Sucursal:</b> {$number} - {$name}<br><b>Dirección:</b> {$street} {$buildingNumber} - {$state}<br><b>Código postal:</b> {$zipCode}<br><b>{$propName}</b>{$telephones}<br><b>Horario:</b> de {$hours} horas<br><b>Ubicación:</b> <a href="https://maps.google.com/?q={$latitude},{$longitude}" target="_blank">¿Cómo llegar?</a><br>'
  },
  ejecutivoNoTel: {
    name: 'ejecutivoNoTel',
    text: '<b>Sucursal:</b> {$number} - {$name}<br><b>Dirección:</b> {$street} {$buildingNumber} - {$state}<br><b>Código postal:</b> {$zipCode}<br><b>Horario:</b> de {$hours} horas<br><b>Ubicación:</b> <a href="https://maps.google.com/?q={$latitude},{$longitude}" target="_blank">¿Cómo llegar?</a><br>'
  },
  delivery: {
    name: 'delivery',
    text: 'Sucursal: {$number} - {$name}. Dirección: {$street} {$buildingNumber} - {$state}<br>'
  }
}
