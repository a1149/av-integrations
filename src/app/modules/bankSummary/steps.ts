export default {
  sinEmailMYA: {
    name: 'sinEmailMYA',
    textApp: 'El envío del resumen es por <b>mail</b>, pero veo que <b>no tenés el tuyo registrado</b>.<br>Por favor, actualizá tus datos haciendo clic {$goto-stack-mya}. Es necesario tener activo el Token de Seguridad para poder hacer la actualización',
    text: 'El envío del resumen es por <b>mail</b>, pero veo que <b>no tenés el tuyo registrado</b>.<br>Por favor, actualizá tus datos haciendo clic {$goto-stack-mya}. Es necesario tener activo el Token de Seguridad para poder hacer la actualización'
  },
  isStaff: {
    name: 'isStaff',
    textApp: 'Veo que sos parte del equipo Santander. 💪<br>Tené en cuenta que los resúmenes de mayor antigüedad los tenés que pedir por One Solution',
    text: 'Veo que sos parte del equipo Santander. 💪<br>Tené en cuenta que los resúmenes de mayor antigüedad los tenés que pedir por One Solution'
  },
  isPrivateBanking: {
    name: 'isPrivateBanking',
    textApp: 'Si necesitás resúmenes de mayor antigüedad, por favor, <b>comunicate con tu ejecutivo de cuentas</b> para que los gestione.',
    text: 'Si necesitás resúmenes de mayor antigüedad, por favor, <b>comunicate con tu ejecutivo de cuentas</b> para que los gestione.'
  },
  notEnoughOld: {
    name: 'notEnoughOld',
    textApp: 'Disculpá, pero no encuentro productos con esta antigüedad. Pero no te preocupes, puedo derivarte con un asesor para que te ayude.',
    text: 'Disculpá, pero no encuentro productos con esta antigüedad. Pero no te preocupes, puedo derivarte con un asesor para que te ayude.',
    derivate: true
  },
  confirmEmail: {
    name: 'confirmEmail',
    textApp: 'Te enviaremos el resumen a tu <b>correo</b> {$mailMYA}.<br>¿Ese es tu mail?',
    text: 'Te enviaremos el resumen a tu <b>correo</b> {$mailMYA}.<br>¿Ese es tu mail?',
    options: [{
      id: 'confirmEmailYes', // sigue en selectProduct
      text: 'Si'
    },
    {
      id: 'confirmEmailNo', // Sigue en changeEmail
      text: 'No'
    }]
  },
  changeEmail: {
    name: 'changeEmail',
    textApp: 'Entonces <b>lo mejor es que actualices tu mail</b>. Hacelo haciendo clic {$goto-stack-mya}.<br>Cuando lo hagas, volvé a escribirme y te ayudaré con tus resúmenes. 💪',
    text: 'Entonces <b>lo mejor es que actualices tu mail</b>. Hacelo haciendo clic {$goto-stack-mya}.<br>Cuando lo hagas, volvé a escribirme y te ayudaré con tus resúmenes. 💪',
    options: [{
      id: 'changeEmailMainMenu', // sigue en CHIT_adios_AV
      text: 'Menú principal'
    }]
  },
  selectProduct: {
    name: 'selectProduct',
    textApp: '¿De cuál <b>producto</b> querés tu resumen?:<br>{$productList}',
    text: '¿De cuál <b>producto</b> querés tu resumen?:<br>{$productList}',
    options: [{
      id: 'selectProductOther', // sigue en otherProduct
      text: 'Otro producto'
    },
    {
      id: 'selectProductMainMenu', // Sigue en CHIT_adios_AV
      text: 'Menú principal'
    }]
  },
  otherProduct: {
    name: 'otherProduct',
    textApp: 'Si el producto que necesitás no está en la lista, puedo derivarte con un asesor para que te ayude.',
    text: 'Si el producto que necesitás no está en la lista, puedo derivarte con un asesor para que te ayude.',
    derivate: true
  },
  selectProductYear: {
    name: 'selectProductYear',
    textApp: '¿De cual <b>año</b> querés tu resumen? Te voy a enviar todos los resúmenes del año que elijas:<br>{$yearProductList}',
    text: '¿De cual <b>año</b> querés tu resumen? Te voy a enviar todos los resúmenes del año que elijas:<br>{$yearProductList}',
    options: [{
      id: 'selectProductYearOther', // sigue en otherProductYear
      text: 'Otro año'
    },
    {
      id: 'selectProductYearMainMenu', // Sigue en CHIT_adios_AV
      text: 'Menú principal'
    }]
  },
  otherProductYear: {
    name: 'otherProductYear',
    textApp: 'Si el año que necesitás no está en la lista, puedo derivarte con un asesor para que te ayude.',
    text: 'Si el año que necesitás no está en la lista, puedo derivarte con un asesor para que te ayude.',
    derivate: true
  },
  confirmBankSummary: {
    name: 'confirmBankSummary',
    textApp: '¡Muy bien! Vamos a enviarte tu resúmenes de tu {$product} del año {$year} a tu mail {$mailMYA}.<br>¿Estás de acuerdo?',
    text: '¡Muy bien! Vamos a enviarte tu resúmenes de tu {$product} del año {$year} a tu mail {$mailMYA}.<br>¿Estás de acuerdo?',
    options: [{
      id: 'confirmBankSummaryYes', // sigue en successBankSummary
      text: 'Si'
    },
    {
      id: 'confirmBankSummaryNo', // sigue en selectProduct
      text: 'No'
    }]
  },
  successBankSummary: {
    name: 'successBankSummary',
    textApp: '¡Excelente! Tus resúmenes te llegarán en 24 horas. 😄',
    text: '¡Excelente! Tus resúmenes te llegarán en 24 horas. 😄',
    options: [{
      id: 'successBankSummaryAnotherOne', // sigue en selectProduct
      text: 'Pedir otro resumen'
    },
    {
      id: 'successBankSummaryMainMenu', // sigue en CHIT_adios_AV
      text: 'Menú principal'
    }]
  }
}
