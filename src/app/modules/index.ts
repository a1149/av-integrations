import { IResponse } from 'av-core-express/lib/interfaces'
import { Response } from 'av-core-express/lib/handlers'
import { IModuleRequest } from '@root/app/interfaces'
import creditCardLimits from '@root/app/modules/creditCardLimits'
import busApi from '@root/app/modules/busApi'

export default async (moduleRequest: IModuleRequest): Promise<IResponse> => {
  const response = new Response()
  try {
    const integrations: any = {
      limites: creditCardLimits,
      sucursales: busApi
    }
    const integration = integrations[moduleRequest.integration]
    const integrationResponse = await integration(moduleRequest.integrationParams)

    return integrationResponse ? response.success(integrationResponse) : response.success(null, 'DEFAULT_RESPONSE')
  } catch (error) {
    return response.error('UNCAUGHT_EXCEPTION', error)
  }
}
