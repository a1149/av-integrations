import { IS_CARDS_LIMITS_UNIFIED } from '@root/app/constant'
import { cardsIncontext } from '@root/app/helpers'
import { ICard, ICardSummary } from '@root/app/interfaces'
import { getCardsResume } from '@root/app/services'
import { cache } from '@root/database'

export default async (nup: number): Promise<any> => {
  const context = await cache.getContext(nup)

  const { token_srvtran: tokenSrvTran } = context

  const cards: ICard[] = await cardsIncontext(context)
  if (!cards.length) return

  let formattedCards = ''

  for (const card of cards) {
    const cardResume: ICardSummary = await getCardsResume(tokenSrvTran, card.number, card.name)
    const purchaseLimit = `${cardResume.cardType === 'Signature' ? 'u$s' : '$'} ` + cardResume.purchaseLimit
    formattedCards += `💳 ${card.title} ${card.lastNumbers}<br>`
    if (cardResume.isUnifiedLimits === IS_CARDS_LIMITS_UNIFIED) {
      formattedCards += `En un pago o en cuotas: ${purchaseLimit}<br>`
    } else {
      const purchaseLimitQuota = `${cardResume.cardType === 'Signature' ? 'u$s' : '$'} ` + cardResume.buyQuotas
      formattedCards += `En un pago: ${purchaseLimit}<br>En cuotas: ${purchaseLimitQuota}<br>`
    }
    formattedCards += `Adelanto de efectivo: $ ${cardResume.advancements}<br><br>`
  }

  const objReplace = {
    '{$titleCards}': cards.length > 1 ? 'tus tarjetas' : 'tu tarjeta',
    '{$cards}': formattedCards
  }
  return objReplace
}
