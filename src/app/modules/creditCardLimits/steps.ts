export default {
  unique: {
    name: 'unique',
    textApp: 'Estos son los límites de {$titleCards} Santander Crédito: <br><br>{$cards} Tené en cuenta que <b>los disponibles se actualizan a partir de la fecha de cierre</b> y <b>al ingresar los pagos.</b> Pueden demorar hasta <b>72 horas hábiles.</b><br><br>Para la <b>tarjeta de débito</b>, podés consultar o modificar los límites de extracción desde Online Banking. Ingresá, abrí el Menú y, en la sección “Solicitudes y trámites”, seleccioná “Tarjetas”.<br><br>Si tenés dudas o consultas, <b>puedo derivarte con un asesor</b> para que te ayude.',
    text: 'Estos son los límites de {$titleCards} Santander Crédito: <br><br>{$cards} Tené en cuenta que <b>los disponibles se actualizan a partir de la fecha de cierre</b> y <b>al ingresar los pagos.</b> Pueden demorar hasta <b>72 horas hábiles.</b><br><br>Para la <b>tarjeta de débito</b>, podés consultar o modificar los límites de extracción, haciendo clic <a data-goto="gotoSolicitudCambioLimiteExtraccion()">acá</a>.<br><br>Si tenés dudas o consultas, <b>puedo derivarte con un asesor</b> para que te ayude.',
    derivate: true
  }
}
