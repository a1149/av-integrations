import JSONSteps from './steps'
import { IIntegrationResponse, IIntegrationRequest } from '@root/app/interfaces/IIntegration'
import { formatResponse, JSONtoObject, textReplace } from '@root/app/helpers'
import formatObjectData from '@root/app/modules/creditCardLimits/formatObjectData'
import { IStep } from '@root/app/interfaces'

export default async (data: IIntegrationRequest): Promise<IIntegrationResponse | undefined> => {
  const step: IStep = JSONtoObject(JSONSteps).unique
  const { channel, nup } = data
  const objReplace = await formatObjectData(nup)
  if (!objReplace) return
  const formattedString = textReplace(step, channel, objReplace)
  return formatResponse(formattedString, step.derivate)
}
