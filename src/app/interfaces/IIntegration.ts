import { IOptions } from '@root/app/interfaces/IStep'

export interface IIntegrationRequest {
  sessionId: string,
  integration: string,
  nup: number,
  channel: 'TBAN' | 'APPM',
  question?: string
}

export interface IIntegrationResponse {
  text: string,
  options: IOptions[] | null,
  derivate: boolean,
  activeSecuentialDialogue: boolean
}

export interface IModuleRequest {
  integration: string,
  integrationParams: any
}
