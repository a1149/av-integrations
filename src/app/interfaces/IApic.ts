/* eslint-disable camelcase */
export interface IApic {
  token_type: string,
  access_token: string,
  expiresIn: number,
  consentedOn: number,
  scope: string,
  'jwt-token': string,
}
