export interface IOptions {
  id: string,
  text: string,
}

export interface IStep {
  name: string,
  textApp?: string,
  text: string,
  options?: IOptions[] | null,
  derivate?: boolean
}
