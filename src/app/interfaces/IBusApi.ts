interface AtmDepositSchedule {
  day: string;
  openingHour: string;
  closingHour: string;
}

interface City {
  code: string;
  name: string;
}

interface Coordinates {
  latitude: number;
  longitude: number;
}

interface Country {
  code: string;
  name: string;
}

interface State {
  code: string;
  name: string;
}

export interface IAddress {
  apartment?: any;
  buildingNumber: string;
  city: City;
  complements?: any;
  coordinates: Coordinates;
  country: Country;
  floor?: any;
  id: number;
  neighborhood: string;
  state: State;
  streetName: string;
  zipCode: string;
}

export interface OpeningHour {
  day: string;
  openingHour: string;
  closingHour: string;
}

interface BranchType {
  id: number;
  description: string;
}

export interface IBusApiResponse {
  id: number;
  atmDepositQuantity: number;
  atmDepositSchedule: AtmDepositSchedule[];
  atmSelfServiceQuantity: number;
  atmExtractionQuantity: number;
  address: IAddress;
  addressId?: number;
  legacyReference: string;
  name: string;
  telephoneNumberIds: number[];
  openingHours: OpeningHour[];
  providedServices: string[];
  zoneId: number;
  branchType: BranchType;
}

export interface ITelResponse {
  id: number,
  type: string,
  areaCode: string,
  countryCode: string,
  number: number,
  extension: string
}
