export interface ICardSummary {
  cardNumber: number,
  cardType: string,
  isUnifiedLimits: 'S' | 'N', // limites unificados o desunificados
  nextClosing: string, // proximo cierre
  nextExpiration: string, // vencimiento proximo
  lastClosing: string, // Cierre último
  lastExpiration: string, // Vencimiento último
  previousClose: string, // Cierre anterior
  previousExpiration: string, // vencimiento Anterior
  minimumPayment: number, // Pago minimo
  purchaseLimit: number, // Limite para Compras
  buyQuotas: number, // compra en cuotas
  purchaseAvailable: number, // Compra disponible
  purchaseQuotasAvailable: number, // Compra en cuotas disponible
  purchaseAdvanceAvailable: number, // Compra delanto disponible
  financing: number, // Financiación
  advancements: number, // Adelantos
  percentage: number, // Porcentaje
  maximumLimitARS: number, // Tope ( limite maximo en pesos )
  lastBalanceARS: number, // Saldo último en pesos
  lastBalanceUSD: number, // Saldo último en dolares
  proviousBalanceARS: number, // Saldo anterior en pesos
  proviousBalanceUSD: number // Saldo anterior en dolares
}

export interface ICard {
  active: boolean,
  title: 'Visa' | 'MasterCard' | 'American Express',
  name: 'VISA' | 'MASTER' | 'AMEX',
  numberKey: 'tarjV' | 'tarjA' | 'tarjM',
  accountKey: 'ctaTV' | 'ctaTA' | 'ctaTM'
  number: number,
  account: number,
  lastNumbers: string
}

export interface IInfoPrisma {
  dataCard: any
}
