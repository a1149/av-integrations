import { IIntegrationRequest, IStep } from '@root/app/interfaces'
import { cache } from '@root/database'
import { CACHE_ACTIVE_INTEGRATION, CACHE_CURRENT_STEP, CACHE_LAST_STEP } from '@root/app/constant'

export class Step {
  public sessionId
  public integration
  public steps

  constructor(data: IIntegrationRequest, steps: any) {
    this.sessionId = data.sessionId
    this.integration = data.integration
    this.steps = steps
  }

  async getCurrent(): Promise<IStep | undefined> {
    return cache.getProperty(this.sessionId, CACHE_CURRENT_STEP)
  }

  async getIntegration(): Promise<string | undefined> {
    return cache.getProperty(this.sessionId, CACHE_ACTIVE_INTEGRATION)
  }

  async getLast(): Promise<IStep | undefined> {
    return cache.getProperty(this.sessionId, CACHE_LAST_STEP)
  }

  async get(name: string): Promise<IStep | undefined> {
    return this.steps[name]
  }

  async setNew(name: string): Promise<boolean> {
    const newStep = await this.get(name)
    const lastCurrent = await this.getCurrent() || undefined
    await cache.saveProperty(this.sessionId, CACHE_ACTIVE_INTEGRATION, this.integration)
    await cache.saveProperty(this.sessionId, CACHE_LAST_STEP, lastCurrent)
    return cache.saveProperty(this.sessionId, CACHE_CURRENT_STEP, newStep)
  }

  async finalDialogue(): Promise<boolean> {
    await cache.delProperty(this.sessionId, CACHE_ACTIVE_INTEGRATION)
    await cache.delProperty(this.sessionId, CACHE_LAST_STEP)
    return cache.delProperty(this.sessionId, CACHE_CURRENT_STEP)
  }
}
