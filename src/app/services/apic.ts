import { IApic } from '@root/app/interfaces'
import { HttpClient, logger } from 'av-core-express/lib/handlers'
import { AxiosRequestConfig } from 'axios'
import { cache } from '@root/database'
import QueryString from 'qs'
import { CACHE_APICTOKEN_INTEGRATIONS } from '@root/app/constant'

export const getToken = async (): Promise<IApic> => {
  let apicToken = null
  try {
    apicToken = await cache.getObject(CACHE_APICTOKEN_INTEGRATIONS)
    if (apicToken) return apicToken

    const path = '/oauth-provider/oauth2/token'
    const data = {
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET,
      grant_type: 'client_credentials',
      scope: 'branches'
    }

    apicToken = await HttpClient.post(path, QueryString.stringify(data), getHttpClientConfiguration())
    if (apicToken.status === 200) {
      const apic: IApic = apicToken.data
      await cache.saveObject(CACHE_APICTOKEN_INTEGRATIONS, apicToken.data)
      return apic
    } else throw new Error('Apic is not defined')
  } catch (error) {
    logger.error('Error en apicToken', error)
    throw error
  }
}

const getHttpClientConfiguration = (): AxiosRequestConfig => {
  return {
    baseURL: process.env.URL_BUSAPI,
    headers: {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'content-type': 'application/x-www-form-urlencoded'
    }
  }
}

module.exports = { getToken }
