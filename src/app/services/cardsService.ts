/* eslint-disable @typescript-eslint/naming-convention */
import xmlToJSON from 'xml-js'
import { ICardSummary } from '@root/app/interfaces'
import { AxiosRequestConfig } from 'axios'
import { HttpClient, logger } from 'av-core-express/lib/handlers'
import { IInfoPrisma } from '@root/app/interfaces/ICard'

export const getCardsResume = async (tokenSrvTran: string, cardNumber: number, cardName: string): Promise<ICardSummary> => {
  const infoPrisma = await getInfoPrisma(tokenSrvTran, cardName, cardNumber, 'ResumenCuenta')
  const card = infoPrisma.dataCard

  const resumeCard: ICardSummary = {
    cardNumber: card.datos.tarjetaActiva._text,
    cardType: card.datos.producto._text.charAt(0).toUpperCase() + card.datos.producto._text.slice(1).toLowerCase(),
    isUnifiedLimits: card.saldoenCuenta._attributes.limitesUnificados,
    nextClosing: card.saldoenCuenta.fechas.fecha[0].cierre._text,
    nextExpiration: card.saldoenCuenta.fechas.fecha[0].vencimiento._text,
    lastClosing: card.saldoenCuenta.fechas.fecha[1].cierre._text,
    lastExpiration: card.saldoenCuenta.fechas.fecha[1].vencimiento._text,
    previousClose: card.saldoenCuenta.fechas.fecha[2].cierre._text,
    previousExpiration: card.saldoenCuenta.fechas.fecha[2].vencimiento._text,
    minimumPayment: card.saldoenCuenta.pagos.pago[0].pesos._text,
    purchaseLimit: card.saldoenCuenta.limites.limite[0].pesos._text,
    buyQuotas: card.saldoenCuenta.limites.limite[1].pesos._text,
    purchaseAvailable: card.saldoenCuenta.limites.limite[2].pesos._text,
    purchaseQuotasAvailable: card.saldoenCuenta.limites.limite[3].pesos._text,
    purchaseAdvanceAvailable: card.saldoenCuenta.limites.limite[4].pesos._text,
    financing: card.saldoenCuenta.limites.limite[5].pesos._text,
    advancements: card.saldoenCuenta.limites.limite[6].pesos._text,
    percentage: card.saldoenCuenta.limites.limite[7].pesos._text,
    maximumLimitARS: card.saldoenCuenta.limites.limite[8].pesos._text,
    lastBalanceARS: card.saldoenCuenta.saldos.saldo[0].pesos._text,
    lastBalanceUSD: card.saldoenCuenta.saldos.saldo[0].dolares._text,
    proviousBalanceARS: card.saldoenCuenta.saldos.saldo[1].pesos._text,
    proviousBalanceUSD: card.saldoenCuenta.saldos.saldo[1].dolares._text
  }

  return resumeCard
}

const getInfoPrisma = async (tokenSrvTran: string, cardName: string, cardNumber: number, typeRequest: string): Promise<IInfoPrisma> => {
  try {
    const cardData = JSON.stringify({
      tarjeta: cardName,
      consulta: typeRequest
    }, null, 0)

    const myBody = '<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"> <x:Header/> <x:Body> <tem:GetInfoPrisma> <tem:token>' + tokenSrvTran + '</tem:token> <tem:jsonWatson>' + cardData + '</tem:jsonWatson> </tem:GetInfoPrisma> </x:Body> </x:Envelope>'
    const requestConfig: AxiosRequestConfig = {
      baseURL: process.env.URL_SRVTRAN,
      headers: {
        'Content-Type': 'text/xml; charset=utf-8',
        SOAPAction: 'http://tempuri.org/IWatsonService/GetInfoPrisma'
      }
    }

    const response = await HttpClient.post('/WatsonService.svc', myBody, requestConfig)

    const jsonRespuesta = JSON.parse(xmlToJSON.xml2json(response.data.replace(/s:Envelope/gi, 's_Envelope').replace(/s:Body/gi, 's_Body'), { compact: true, spaces: 4 }))
    const jsonResult = JSON.parse(jsonRespuesta.s_Envelope.s_Body.GetInfoPrismaResponse.GetInfoPrismaResult._text)
    const bufferResponse = Buffer.from(jsonResult.Response, 'base64')
    const xmlResponse = bufferResponse.toString('ascii')
    let dataResult = JSON.parse(xmlToJSON.xml2json(xmlResponse.replace(/s:Envelope/gi, 's_Envelope').replace(/s:Body/gi, 's_Body'), { compact: true, spaces: 4 })).tarjetas.tarjeta

    if (Array.isArray(dataResult)) {
      dataResult = dataResult.find((json: any) => json.document.datos.tarjetaActiva._text === cardNumber)
    }

    return { dataCard: dataResult.document }
  } catch (error) {
    logger.error('[CARD] Error in get card resume', error)
    throw error
  }
}
