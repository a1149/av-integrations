import { IAddress, IApic, IBusApiResponse, ITelResponse } from '@root/app/interfaces'
import { getToken } from '@root/app/services'
import { HttpClient, logger } from 'av-core-express/lib/handlers'
import { AxiosRequestConfig } from 'axios'

export const getBranchInfo = async (branchId: string): Promise<IBusApiResponse> => {
  try {
    const apic = await getToken()
    const path = '/bus/api/branches/'
    const params = {
      legacyReference: branchId.slice(-3)
    }
    const busResponse = await HttpClient.get(path, getHttpClientConfiguration(apic, params))
    const busData: IBusApiResponse = busResponse.data.results[0]
    if (busData) {
      return busData
    } else throw new Error('Response is not defined')
  } catch (error) {
    logger.error('[BUS] getBranchInfo', error)
    throw error
  }
}

export const getTelInfo = async (telList: number[]): Promise<ITelResponse[] | undefined> => {
  try {
    const apic = await getToken()
    const response: ITelResponse[] = []
    for (const telId of telList) {
      const path = '/bus/api/telephones/' + telId
      const busResponse = await HttpClient.get(path, getHttpClientConfiguration(apic))
      response.push(busResponse.data)
    }
    return response
  } catch (error) {
    logger.error('[BUS] getTelInfoByIds', error)
    return undefined
  }
}

export const getAddressInfo = async (addressId: number): Promise<IAddress> => {
  try {
    const apic = await getToken()
    const path = '/bus/api/addresses/' + addressId
    const response = await HttpClient.get(path, getHttpClientConfiguration(apic))
    return response.data
  } catch (error) {
    logger.error('[BUS] getAddressesInfo', error)
    throw error
  }
}

const getHttpClientConfiguration = (apic: IApic, params?: any): AxiosRequestConfig => {
  return {
    baseURL: process.env.URL_BUSAPI,
    headers: {
      accept: 'application/json',
      authorization: 'Bearer ' + apic.access_token,
      'x-ibm-client-id': process.env.CLIENT_ID,
      'x-authorization': 'Bearer ' + apic['jwt-token']
    },
    params: params
  }
}
