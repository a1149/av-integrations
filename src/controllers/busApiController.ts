import { Request, Response } from 'av-core-express/lib/interfaces'
import modules from '@root/app/modules'
import { Response as ModuleResponse } from 'av-core-express/lib/handlers'
import { validateRequestJoiBusApi } from '@root/app/helpers'
import { IModuleRequest } from '@root/app/interfaces'

export default async (request: Request, apiResponse: Response): Promise<Response> => {
  try {
    await validateRequestJoiBusApi(request.query)

    const integrationParams: any = {
      channel: request.query.channel,
      branchId: request.query.branchId,
      origin: request.query.origin
    }

    const moduleRequest: IModuleRequest = {
      integration: request.query.integration as string,
      integrationParams: integrationParams
    }
    const integration = await modules(moduleRequest)
    return apiResponse.status(integration.statusCode).json(integration)
  } catch (error) {
    const codeError = error.isJoi ? 'INVALID_BODY' : 'UNCAUGHT_EXCEPTION'
    const errorResponse = new ModuleResponse().error(codeError, error)
    return apiResponse.status(errorResponse.statusCode).json(errorResponse)
  }
}
