import { Request as ReqExpress, Response as ResExpress } from 'av-core-express/lib/interfaces'
import modules from '@root/app/modules'
import { Response as ResponseHandler } from 'av-core-express/lib/handlers'
import { validateRequestJoi } from '@root/app/helpers'
import { IModuleRequest } from '@root/app/interfaces'
import { INVALID_BODY, UNCAUGHT_EXCEPTION } from '@root/app/constant'

export default async (req: ReqExpress, res: ResExpress): Promise<ResExpress> => {
  try {
    await validateRequestJoi(req.query)
    const myReq: any = {
      sessionId: req.query.sessionId,
      integration: req.query.integration,
      nup: req.query.nup,
      channel: req.query.channel
    }
    const moduleRequest: IModuleRequest = {
      integration: myReq.integration,
      integrationParams: myReq
    }
    const integration = await modules(moduleRequest)
    return res.status(integration.statusCode).json(integration)
  } catch (error) {
    const codeError = error.isJoi ? INVALID_BODY : UNCAUGHT_EXCEPTION
    const errorRes = new ResponseHandler().error(codeError, error)
    return res.status(errorRes.statusCode).json(errorRes)
  }
}
