import { Request, Response } from 'av-core-express/lib/interfaces'

export default (request: Request, response: Response): Response => response.status(200).json({ status: 'active' })
