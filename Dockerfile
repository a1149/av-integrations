FROM gitlab.ar.bsch:4567/santander-tecnologia/dockerbaseimages/nodejs-openshift:v12

COPY . .

RUN npm install --production

ENV PORT 8080
EXPOSE $PORT

CMD [ "npm", "start" ]
