import { getToken } from '../src/app/services/index'

jest.mock('../src/database/cache', () => {
  return {
    getObject: (): any => Promise.resolve(false),
    saveObject: (): any => Promise.resolve(true)
  }
})

describe('Apic', () => {
  it('OK', async () => {
    const res = await getToken()
    expect(res).toBeDefined()
  })
})
