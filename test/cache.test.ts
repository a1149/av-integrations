import { cache } from '../src/database/index'

describe('Cache test', () => {
  it('getObject', async () => {
    const cacheObject = {
      test1: 'test1',
      test2: 'test2'
    }
    await cache.saveObject('cacheTest', cacheObject)
    const res = await cache.getObject('cacheTest')
    expect(res.test1).toEqual('test1')
  })
})
