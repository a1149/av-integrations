import { getAddressInfo, getBranchInfo, getTelInfo } from '../src/app/services/busApiService'

describe('BusApiService', () => {
  it('getBranchInfo-OK', async () => {
    const branchId = '000'
    const res = await getBranchInfo(branchId)
    expect(res.legacyReference).toEqual(branchId)
  })
})

describe('BusApiService', () => {
  it('getBranchInfo-ERROR', async () => {
    const branchId = '0654'
    try {
      await getBranchInfo(branchId)
    } catch (e) {
      expect(e.message).toEqual('Response is not defined')
    }
  })
})

describe('BusApiService', () => {
  it('getTelInfo-OK', async () => {
    const telList = [111, 222, 333]
    const res = await getTelInfo(telList)
    expect(res[0].id).toEqual(telList[0])
  })
})

describe('BusApiService', () => {
  it('getTelInfo-ERROR404', async () => {
    const telList = [1]
    const res = await getTelInfo(telList)
    expect(res).toBeUndefined()
  })
})

describe('BusApiService', () => {
  it('getAddressInfo-OK', async () => {
    const addressId = 1
    const res = await getAddressInfo(addressId)
    expect(res.id).toEqual(addressId)
  })
})

describe('BusApiService', () => {
  it('getAddressInfo-404', async () => {
    const addressId = 1234
    try {
      await getAddressInfo(addressId)
    } catch (e) {
      expect(e.response.status).toEqual(404)
    }
  })
})
