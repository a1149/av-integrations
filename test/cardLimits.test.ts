/* eslint-disable quotes */
import app from '../src/app'
import supertest from 'supertest'
import { cache } from '../src/database/index'
import { CHANNEL_TBAN } from '../src/app/constant'
// import { getCardsResume } from '../src/app/services/index'

const nup = '00989809'
const sessionId = 'LimitsTest'
const integration = 'limites'
const dataPrisma = { data: "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><GetInfoPrismaResponse xmlns=\"http://tempuri.org/\"><GetInfoPrismaResult>{\"Response\":\"PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSIgc3RhbmRhbG9uZT0ieWVzIj8+Cjx0YXJqZXRhcz48dGFyamV0YT48ZG9jdW1lbnQgc2Vzc2lvbklEPSJ0bFRfZkNTU2xKSmN1S2dNM0RpcUhnTm4iPjxkYXRvcyBpZD0iNzM5MjY0OTkiPjxhZmZpbml0eUdyb3VwPjMzOTwvYWZmaW5pdHlHcm91cD48YXBlbGxpZG8+R0FNQVpPPC9hcGVsbGlkbz48Y2F0ZWdvcmlhPjA8L2NhdGVnb3JpYT48Y29kVGlwb1RhcmpldGE+Q0hJUCBFTVYgQy9DT05UQUM8L2NvZFRpcG9UYXJqZXRhPjxjb2RpZ29TdWN1cnNhbD4xNDE8L2NvZGlnb1N1Y3Vyc2FsPjxjdWVudGE+MzE4Njc1NDQ8L2N1ZW50YT48ZmVjaGFEZXNkZT4wMS8wOC8yMDE3PC9mZWNoYURlc2RlPjxmZWNoYU5hY2ltaWVudG8+MjEvMDYvMTk2MTwvZmVjaGFOYWNpbWllbnRvPjxoYWJpZW50ZT5HQU1BWk8vRUxJTyBEPC9oYWJpZW50ZT48bm9tYnJlPkVMSU8gREFOSUVMIDwvbm9tYnJlPjxkb2N1bWVudG8+MTQxODc2Njg8L2RvY3VtZW50bz48cHJvZHVjdG8+Q2xhc3NpYyBJbnRlcm5hY2lvbmFsPC9wcm9kdWN0bz48dGFyamV0YUFjdGl2YT40NTA5NzkwMTM5MzczMDY2PC90YXJqZXRhQWN0aXZhPjx0YXJqZXRhUHJvZHU+Q1IwMTA0PC90YXJqZXRhUHJvZHU+PHRlbGVmb25vPjYyMjc5OTA8L3RlbGVmb25vPjx0aXBvRG9jdW1lbnRvIGNvZGlnbz0iMSI+RE5JPC90aXBvRG9jdW1lbnRvPjx0aXBvVGFyamV0YURldGFsbGU+Q0hJUCBFTVYgQy9DT05UQUM8L3RpcG9UYXJqZXRhRGV0YWxsZT48dGl0dWxhcj5HQU1BWk8vRUxJTyBEPC90aXR1bGFyPjx2ZW5jaW1pZW50bz4yMzA3PC92ZW5jaW1pZW50bz48d2Vic2l0ZS8+PC9kYXRvcz48c2FsZG9lbkN1ZW50YSBsaW1pdGVzVW5pZmljYWRvcz0iUyI+PGZlY2hhcz48ZmVjaGEgZGVzY3JpcGNpb249InByb3hpbW8iPjxjaWVycmU+MTAvMDYvMjAyMTwvY2llcnJlPjx2ZW5jaW1pZW50bz4yMi8wNi8yMDIxPC92ZW5jaW1pZW50bz48L2ZlY2hhPjxmZWNoYSBkZXNjcmlwY2lvbj0idWx0aW1vIj48Y2llcnJlPjA2LzA1LzIwMjE8L2NpZXJyZT48dmVuY2ltaWVudG8+MTcvMDUvMjAyMTwvdmVuY2ltaWVudG8+PC9mZWNoYT48ZmVjaGEgZGVzY3JpcGNpb249ImFudGVyaW9yIj48Y2llcnJlPjA4LzA0LzIwMjE8L2NpZXJyZT48dmVuY2ltaWVudG8+MTkvMDQvMjAyMTwvdmVuY2ltaWVudG8+PC9mZWNoYT48L2ZlY2hhcz48cGFnb3M+PHBhZ28gZGVzY3JpcGNpb249Im1pbmltbyI+PHBlc29zPjMyMCwwMDwvcGVzb3M+PGRvbGFyZXM+MCwwMDwvZG9sYXJlcz48L3BhZ28+PHBhZ28gZGVzY3JpcGNpb249Im1pbmltby1hbnQiPjxwZXNvcz43MCwwMDwvcGVzb3M+PGRvbGFyZXM+MCwwMDwvZG9sYXJlcz48L3BhZ28+PC9wYWdvcz48bGltaXRlcz48bGltaXRlIGRlc2NyaXBjaW9uPSJjb21wcmEiPjxwZXNvcz4xNS4wMDAsMDA8L3Blc29zPjwvbGltaXRlPjxsaW1pdGUgZGVzY3JpcGNpb249ImNvbXByYWN1b3RhcyI+PHBlc29zPjIyLjUwMCwwMDwvcGVzb3M+PC9saW1pdGU+PGxpbWl0ZSBkZXNjcmlwY2lvbj0iY29tcHJhZGlzcCI+PHBlc29zPjEyLjg3MSw4MTwvcGVzb3M+PC9saW1pdGU+PGxpbWl0ZSBkZXNjcmlwY2lvbj0iY29tcHJhY3VvdGFzZGlzcCI+PHBlc29zPjEyLjg3MSw4MTwvcGVzb3M+PC9saW1pdGU+PGxpbWl0ZSBkZXNjcmlwY2lvbj0iY29tcGFkZWxkaXNwIj48cGVzb3M+My4wMDAsMDA8L3Blc29zPjwvbGltaXRlPjxsaW1pdGUgZGVzY3JpcGNpb249ImZpbmFuY2lhY2lvbiI+PHBlc29zPjEzLjUwMCwwMDwvcGVzb3M+PC9saW1pdGU+PGxpbWl0ZSBkZXNjcmlwY2lvbj0iYWRlbGFudG9zIj48cGVzb3M+My4wMDAsMDA8L3Blc29zPjwvbGltaXRlPjxsaW1pdGUgZGVzY3JpcGNpb249InBvcmNlbnRhamUiPjxwZXNvcz4wLDAwPC9wZXNvcz48L2xpbWl0ZT48bGltaXRlIGRlc2NyaXBjaW9uPSJ0b3BlIj48cGVzb3M+MCwwMDwvcGVzb3M+PC9saW1pdGU+PC9saW1pdGVzPjxzYWxkb3M+PHNhbGRvIGRlc2NyaXBjaW9uPSJ1bHRpbWEiPjxwZXNvcz42LjMyNSw4OTwvcGVzb3M+PGRvbGFyZXM+MCwwMDwvZG9sYXJlcz48L3NhbGRvPjxzYWxkbyBkZXNjcmlwY2lvbj0iYW50ZXJpb3IiPjxwZXNvcz4xLjUxOCwwMDwvcGVzb3M+PGRvbGFyZXM+MCwwMDwvZG9sYXJlcz48L3NhbGRvPjwvc2FsZG9zPjx0YXNhcz48dGFzYSBkZXNjcmlwY2lvbj0iYW51YWwiPjxwZXNvcz40MywwMDAwPC9wZXNvcz48ZG9sYXJlcz4wLDAwMDA8L2RvbGFyZXM+PC90YXNhPjx0YXNhIGRlc2NyaXBjaW9uPSJtZW5zdWFsIj48cGVzb3M+Myw1MzAwPC9wZXNvcz48ZG9sYXJlcz4wLDAwMDA8L2RvbGFyZXM+PC90YXNhPjwvdGFzYXM+PC9zYWxkb2VuQ3VlbnRhPjwvZG9jdW1lbnQ+PC90YXJqZXRhPjx0YXJqZXRhPjxkb2N1bWVudCBzZXNzaW9uSUQ9InRsVF9mQ1NTbEpKY3VLZ00zRGlxSGdObiI+PGRhdG9zIGlkPSI3MzkyNjQ5OSI+PGFmZmluaXR5R3JvdXA+MzM5PC9hZmZpbml0eUdyb3VwPjxhcGVsbGlkbz5HQU1BWk88L2FwZWxsaWRvPjxjYXRlZ29yaWE+MTwvY2F0ZWdvcmlhPjxjb2RUaXBvVGFyamV0YT5DSElQIEVNViBDL0NPTlRBQzwvY29kVGlwb1RhcmpldGE+PGNvZGlnb1N1Y3Vyc2FsPjE0MTwvY29kaWdvU3VjdXJzYWw+PGN1ZW50YT4zMTg2NzU0NDwvY3VlbnRhPjxmZWNoYURlc2RlPjAxLzA4LzIwMTc8L2ZlY2hhRGVzZGU+PGZlY2hhTmFjaW1pZW50bz4yNy8wOC8xOTkwPC9mZWNoYU5hY2ltaWVudG8+PGhhYmllbnRlPkdBTUFaTy9NQVJJQSBGTE9SRU48L2hhYmllbnRlPjxub21icmU+RUxJTyBEQU5JRUwgPC9ub21icmU+PGRvY3VtZW50bz4xNDE4NzY2ODwvZG9jdW1lbnRvPjxwcm9kdWN0bz5DbGFzc2ljIEludGVybmFjaW9uYWw8L3Byb2R1Y3RvPjx0YXJqZXRhQWN0aXZhPjQ1MDk3OTAxMzkzNzMwNzQ8L3RhcmpldGFBY3RpdmE+PHRhcmpldGFQcm9kdT5DUjAxMDQ8L3RhcmpldGFQcm9kdT48dGVsZWZvbm8+NjIyNzk5MDwvdGVsZWZvbm8+PHRpcG9Eb2N1bWVudG8gY29kaWdvPSIxIj5ETkk8L3RpcG9Eb2N1bWVudG8+PHRpcG9UYXJqZXRhRGV0YWxsZT5DSElQIEVNViBDL0NPTlRBQzwvdGlwb1RhcmpldGFEZXRhbGxlPjx0aXR1bGFyPkdBTUFaTy9NQVJJQSBGTE9SRU48L3RpdHVsYXI+PHZlbmNpbWllbnRvPjIzMDc8L3ZlbmNpbWllbnRvPjx3ZWJzaXRlLz48L2RhdG9zPjxzYWxkb2VuQ3VlbnRhPjxmZWNoYXM+PGZlY2hhIGRlc2NyaXBjaW9uPSJwcm94aW1vIj48Y2llcnJlPjEwLzA2LzIwMjE8L2NpZXJyZT48dmVuY2ltaWVudG8+MjIvMDYvMjAyMTwvdmVuY2ltaWVudG8+PC9mZWNoYT48ZmVjaGEgZGVzY3JpcGNpb249InVsdGltbyI+PGNpZXJyZT4wNi8wNS8yMDIxPC9jaWVycmU+PHZlbmNpbWllbnRvPjE3LzA1LzIwMjE8L3ZlbmNpbWllbnRvPjwvZmVjaGE+PGZlY2hhIGRlc2NyaXBjaW9uPSJhbnRlcmlvciI+PGNpZXJyZT4wOC8wNC8yMDIxPC9jaWVycmU+PHZlbmNpbWllbnRvPjE5LzA0LzIwMjE8L3ZlbmNpbWllbnRvPjwvZmVjaGE+PC9mZWNoYXM+PC9zYWxkb2VuQ3VlbnRhPjwvZG9jdW1lbnQ+PC90YXJqZXRhPjwvdGFyamV0YXM+Cg==\",\"Resultado\":\"0\",\"Mensaje\":null,\"Token\":\"9c0e0318-1957-453d-8299-3919bb996269\"}</GetInfoPrismaResult></GetInfoPrismaResponse></s:Body></s:Envelope>" }

jest.mock('axios', () => {
  return {
    create: jest.fn(() => ({
      post: (): any => (dataPrisma),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() }
      }
    }))
  }
})

beforeAll(async (done) => {
  const context = {
    token_srvtran: 'tokenTest',
    marcTarjV: 1,
    marcTarjA: 0,
    marcTarjM: 0,
    ctaTV: '141-31867544',
    tarjV: '4509790139373066'
  }
  cache.saveObject(nup + '_context', context)
  done()
})

describe('Get Integration', () => {
  it('CCLimits-OK', async () => {
    const res = await supertest(app)
      .get('/integrations')
      .query({
        sessionId,
        integration,
        nup,
        channel: CHANNEL_TBAN
      })
    expect(res.status).toEqual(200)
  })
})
