/* eslint-disable no-unused-vars */
import dotenv from 'dotenv'

const env = 'test'
const envConfig = dotenv.config({ path: `./env/${env}` }).parsed
dotenv.config({ path: `./env/${env}` })
