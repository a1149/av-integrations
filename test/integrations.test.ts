/* eslint-disable quotes */
import app from '../src/app'
import supertest from 'supertest'
import { CHANNEL_TBAN } from '../src/app/constant'

describe('Get Integration', () => {
  it('ERROR response', async () => {
    const res = await supertest(app)
      .get('/integrations')
      .query({
        sessionId: 'CCLimitsTest',
        nup: '00024513',
        channel: CHANNEL_TBAN
      })
    expect(res.status).toEqual(400)
  })
})

describe('Get Integration', () => {
  it('500 response', async () => {
    const res = await supertest(app)
      .get('/integrations')
      .query({
        sessionId: 'CCLimitsTest',
        integration: 'noExiste',
        nup: '00024513',
        channel: CHANNEL_TBAN
      })
    expect(res.status).toEqual(500)
  })
})
