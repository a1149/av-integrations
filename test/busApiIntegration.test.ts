/* eslint-disable quotes */
import app from '../src/app'
import supertest from 'supertest'
import { CHANNEL_APPM, CHANNEL_TBAN } from '../src/app/constant/index'

describe('BusApiIntegration', () => {
  it('OK-TBAN', async () => {
    const res = await supertest(app)
      .get('/bus-api')
      .query({
        integration: 'sucursales',
        branchId: '048',
        channel: CHANNEL_TBAN,
        origin: 'ejecutivo'
      })
    expect(res.status).toEqual(200)
  })
})

describe('BusApiIntegration', () => {
  it('OK-APPM', async () => {
    const res = await supertest(app)
      .get('/bus-api')
      .query({
        integration: 'sucursales',
        branchId: '000',
        channel: CHANNEL_APPM,
        origin: 'delivery'
      })
    expect(res.status).toEqual(200)
  })
})

describe('BusApiIntegration', () => {
  it('ERROR-NotExistingBranchId', async () => {
    const res = await supertest(app)
      .get('/bus-api')
      .query({
        integration: 'sucursales',
        branchId: '0001',
        channel: CHANNEL_TBAN,
        origin: 'ejecutivo'
      })
    expect(res.status).toEqual(500)
  })
})

describe('BusApiIntegration', () => {
  it('Error 400', async () => {
    const res = await supertest(app)
      .get('/bus-api')
      .query({
        integration: 'sucursales',
        channel: CHANNEL_TBAN
      })
    expect(res.status).toEqual(400)
  })
})
