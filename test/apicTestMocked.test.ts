import { CACHE_APICTOKEN_INTEGRATIONS } from '../src/app/constant'
import { getToken } from '../src/app/services/index'
import { cache } from '../src/database'

beforeAll(async (done) => {
  cache.delObject(CACHE_APICTOKEN_INTEGRATIONS)
  done()
})

jest.mock('axios', () => {
  return {
    create: jest.fn(() => ({
      post: (): any => ({ status: 401 }),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() }
      }
    }))
  }
})

describe('Apic', () => {
  it('ERROR-ApicNotDefined', async () => {
    try {
      await getToken()
    } catch (e) {
      expect(e.message).toEqual('Apic is not defined')
    }
  })
})
