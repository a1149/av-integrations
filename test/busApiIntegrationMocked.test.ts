/* eslint-disable quotes */
import app from '../src/app'
import supertest from 'supertest'
import { CHANNEL_TBAN } from '../src/app/constant'

jest.mock('axios', () => {
  return {
    create: jest.fn(() => ({
      get: (): any => ({ data: { results: [{ addressId: 101 }] }, statusCode: 200 }),
      interceptors: {
        request: { use: jest.fn(), eject: jest.fn() },
        response: { use: jest.fn(), eject: jest.fn() }
      }
    }))
  }
})

describe('BusApiIntegration', () => {
  it('ERROR-WithAddressId', async () => {
    const res = await supertest(app)
      .get('/bus-api')
      .query({
        integration: 'sucursales',
        branchId: '048',
        channel: CHANNEL_TBAN,
        origin: 'ejecutivo'
      })
    expect(res.status).toEqual(500)
  })
})
