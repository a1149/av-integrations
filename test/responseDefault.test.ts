/* eslint-disable quotes */
import app from '../src/app'
import supertest from 'supertest'
import { cache } from '../src/database/index'
import { CHANNEL_TBAN } from '../src/app/constant'
// import { getCardsResume } from '../src/app/services/index'

const nup = '0098980321'
const sessionId = 'LimitsTestDefault'
const integration = 'limites'

beforeAll(async (done) => {
  const context = {
    token_srvtran: 'tokenTest',
    marcTarjV: 0,
    marcTarjA: 0,
    marcTarjM: 0
  }
  cache.saveObject(nup + '_context', context)
  done()
})

describe('Get Integration', () => {
  it('CCLimits-DEFAULT', async () => {
    const res = await supertest(app)
      .get('/integrations')
      .query({
        sessionId,
        integration,
        nup,
        channel: CHANNEL_TBAN
      })
    expect(res.body.code).toEqual('DEFAULT_RESPONSE')
  })
})
