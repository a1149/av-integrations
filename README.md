## Node Express template project

### CI Pipeline
Actualmente la Pipeline para Node cuenta de 4 pasos estándar,
- Package
- Test
- Quality
- Buildimage

Por otro lado, la pipeline utiliza la imagen más actualizada disponible de node para los steps de _Package_ y _Test_, configurada con HTTP\_PROXY, HTTPS\_PROXY y NO\_PROXY.

##### Package
Ejecuta `npm install`, instalando así todas las dependencias en el path _node\_modules_. 

##### Test
Ejecuta `npm test`, utilizando el mismo path previo: _node\_modules_. En este proyecto, el comando utiliza `jest` para ejecutar los tests. Esto puede modificarse desde el archivo `launch.json`. 

##### Quality
Utiliza SonarQubepara analizar Code Smells, Vulnerabilidades, Coverage de los Tests, etc. Se puede ver el resultado del último Quality Gate en cada Merge Request, si se desea. Para realizar esto, por favor comunicarse con el equipo de DevOps, ya que requiere configuración de Administrador de Sonar. 

##### Build-image
Utiliza una imagen de [Docker in Docker (dind)](https://hub.docker.com/_/docker/) para a su vez crear una imagen que contenga el código buildeado, y la sube al Container Registry local del proyecto, dejándola lista para hacer el traspaso a OKD, o a cualquier otro lugar donde se desee deployar la misma. 

##### Deploy
Utilizando un template de Openshift, y variables declaradas en un archivo con esa función, este step importa a la instancia de Openshift apropiada la imagen generada en el paso anterior, y luego la lanza, asegurándose que ésta cumpla con las especificaciones del template de Openshift. 

A tener en cuenta, el path relativo al mismo debe estar definido en la variable `OKD_TEMPLATE_NAME: <path>/<file_name>` en el archivo `ci-variables.yml`, así como el path relativo al archivo de variables de Openshift debe estar definido en la variable `OKD_TEMPLATE_PARAMS: <path>/<file_name>`. Por default, ambos archivos se encuentran contenidos actualmente en la carpeta `okd`, y son funcionales a este proyecto. A tener en cuenta, en caso de querer utilizar el template, también es obligatorio setear la variable `OKD_PROJECT` en `ci-variables.yml`, así como las variables en `okd/okd-params.env`, para que estas reflejen la data del proyecto actual. 

Como paso final, es necesario también crear un Deploy Token, desde _Settings_ -> _Repository_ -> _Deploy Token_. El mismo debe tener el nombre __gitlab-deploy-token__, y permisos __read\_registry__. No es necesario completar las otras variables ni almacenar el token.



### GitFlow
La estrategia de branches propuesta para todos los equipos es Gitflow. En la misma, existen las siguientes branches:
- Master.
- Staging.
- Development.
- Features.

El movimiento entre branches es exclusivamente a través de Merge Requests, y requiere que los mismos se aprueben por miembros del equipo, acorde a la branch de destino.

##### Master
Es la única branch no volátil del repositorio. Contiene el código que se deploya en producción, y debe tener un tag acorde al mismo. Toda branch partirá siempre a partir de ésta.

##### Features
Aquí se programa la solución. Como se comentó antes, su origen es `master`, y su nombre debe comenzar con la palabra `feature`. Una vez el código esté listo, __el desarrollador__ debe crear un Merge Request a `development`. El merge request deberá ser aprobado por __otro miembro del equipo que no haya participado de este desarrollo__. __El Team Leader__ es el encargado de Mergear el código, una vez la pipeline asociada al merge request sea exitosa y el mismo esté aprobado. Una vez mergeado el código, la branch debe ser eliminada.

##### Development
Aquí se solucionan los conflictos entre las distintas branches de feature. Como se comentó antes, su origen es `master`, y su nombre debe comenzar con la palabra `dev`. Recomendamos `development` para su nombre, aunque acepta alternativas. Una vez el código esté listo, __el desarrollador__ debe crear un Merge Request a `staging`. El merge request deberá ser aprobado por __el Team Leader__. __Un representante de Quality Assurance__ es el encargado de Mergear el código, una vez la pipeline asociada al merge request sea exitosa y el mismo esté aprobado. Una vez mergeado el código, la branch debe ser eliminada.

##### Staging
Aquí se prepara el entregable, mezclando el código de `development` con `master`, y resolviendo posibles conflictos por eventuales hotfixes. Como se comentó antes, su origen es `master`, y su nombre debe ser `staging`. Una vez el código esté listo, __el QA Leader__ debe crear un Merge Request a `staging`. El merge request deberá ser aprobado por __el Head o el Sponsor__. __El QA Leader__ es el encargado de Mergear el código, una vez la pipeline asociada al merge request sea exitosa y el mismo esté aprobado. Una vez mergeado el código, la branch debe ser eliminada. Staging es el step en el cual se deben ejecutar las User Acceptance Tests.

##### Hotfixes
Aquí se programan arreglos ad-hoc de vicios ocultos que hubieran llegado inadvertidamente a `master`. Como se comentó antes, su origen es `master`, y su nombre debe comenzar con la palabra `hotfix`. Una vez el código esté listo, __el QA Leader__ debe crear un Merge Request a `staging`. El merge request deberá ser aprobado por __el Head o el Sponsor__. __El QA Leader__ es el encargado de Mergear el código, una vez la pipeline asociada al merge request sea exitosa y el mismo esté aprobado. Una vez mergeado el código, la branch debe ser eliminada. En el caso de que existiera una branch de `staging` luego de mergear el hotfix a master, __el desarrollador__ debe crear un Merge Request de `master` a `staging`. El merge request deberá ser aprobado por __el Team Leader__. __Un representante de Quality Assurance__ es el encargado de Mergear el código, una vez la pipeline asociada al merge request sea exitosa y el mismo esté aprobado.

### Ejecución de la Pipeline
Existen 2 pipelines distintas:
- Merge Request Pipeline.
- Branch Pipeline.

Es importante nombrar que las branches de feature no tienen pipeline asociada, no así sus Merge Requests.

##### Merge Request Pipeline
Ejecuta pruebas estáticas del código: QA (`Sonar`), SAST (`Kiuwan`), y tests (`Unit`, `Functional`, `Integration` y `Smoke`). Es propia de cada Merge Request.

##### Branch Pipeline
Deploya y ejecuta pruebas dinámicas del código: DAST (sólamente `Staging`), Build-Image (`Development` y `Staging`), y OKD-Deploy (`Development` y `Staging`). En el caso de `master`, el deploy ocurre promoviendo el container que ya está corriendo actualmente en `Staging`.

